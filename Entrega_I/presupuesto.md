# PRESUPUESTO 
El sistema contara con las siguientes funciones:
* Debera administrar usuarios, los cuales tendran sus sitios propios y una lista de lugares favoritos. Los usuarios registrados podran dejar comentarios tanto en lugares como en cada plato y podran calificar cada uno.
* Debera administrar sitios como restaurantes, bares y otros lugares de dicha indole. Mostrara las caracteristicas principales, los medios de pago, informacion para contactar con el lugar e imagenes.
* Debera administrar el menu de cada lugar, agregando informacion de platos y bebidas. Debera incluir imagenes, informacion nutricional y para que publico esta dirigido.
* Los usuarios registrados podran agregar lugares propios y platos y la informacion será evaluada/moderada antes de ser subida al sitio. Podran buscar por nombre, por cercania, por localidad.
* Los usuarios podran compartir en diferentes redes sociales los restaurantes y platos.
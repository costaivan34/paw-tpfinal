var sectionBase = sectionBase || {},
    timer,
    lista = [];

armarCarrousel = function (contenedor) {
    window.addEventListener("DOMContentLoaded", function () {
        sectionBase = document.getElementById(contenedor);
        cargarDestacados();
        crearNavegador();
        timerMover();
    });
};

//aca vendrian los datos por parametros y se completa dinamicamente
function cargarDestacados() {
    lista = [{
            "fotoDestacadoPath": "./public/res/foto/foto1.jpg",
            "restaurante": "Restaurante X",
        },
        {
            "fotoDestacadoPath": "./public/res/foto/foto2.jpg",
            "restaurante": "Restaurante X",
        },
        {
            "fotoDestacadoPath": "./public/res/foto/foto3.jpg",
            "restaurante": "Restaurante X",
        },
        {
            "fotoDestacadoPath": "./public/res/foto/foto4.jpg",
            "restaurante": "Restaurante X",
        },
        {
            "fotoDestacadoPath": "./public/res/foto/foto5.jpg",
            "restaurante": "Restaurante X",
        }
    ];
}

function crearNavegador() {
    var h2Titulo = document.createElement("h2");
    h2Titulo.textContent = "DESTACADOS";
    var listaDestacadosUL = document.createElement("ul");
    listaDestacadosUL.id = "listaDestacados";
    for (let i = 0; i < lista.length; i++) {
        var itemDestacadoLI = document.createElement("li");
        itemDestacadoLI.classList.add("liCarrousel");
        var aItemDestacado = document.createElement("a");
        aItemDestacado.setAttribute("href", "/");
        var itemFoto = document.createElement("img");
        itemFoto.classList.add("fotoDestacados");
        itemFoto.setAttribute("src", lista[i]["fotoDestacadoPath"]);
        var h3Descripcion = document.createElement("h3");
        h3Descripcion.textContent = lista[i]["restaurante"];
        if (i < 1) {
            itemDestacadoLI.classList.add("activo");
        } else {
            itemDestacadoLI.classList.add("inactivo");
        }
        aItemDestacado.appendChild(itemFoto);
        aItemDestacado.appendChild(h3Descripcion);
        itemDestacadoLI.appendChild(aItemDestacado);
        listaDestacadosUL.appendChild(itemDestacadoLI);
    }

    //creo botones prev next
    var aPrev = document.createElement("a");
    aPrev.innerHTML = "&#10094;";
    aPrev.classList.add("prev");
    aPrev.addEventListener("click", function () {
        timerMover();
        moverSlide(-1);
    });
    var aNext = document.createElement("a");
    aNext.innerHTML = "&#10095;";
    aNext.classList.add("next");
    aNext.addEventListener("click", function () {
        timerMover();
        moverSlide(1);
    });
    sectionBase.appendChild(h2Titulo);
    sectionBase.appendChild(aPrev);
    sectionBase.appendChild(listaDestacadosUL);
    sectionBase.appendChild(aNext);
}

function moverSlide(orientacion) {
    var listaUl = document.getElementById("listaDestacados");
    var listaLi = document.getElementsByClassName("liCarrousel");
    for (let i = 0; i < listaLi.length; i++) {
        listaLi[i].classList.remove("derecha");
        listaLi[i].classList.remove("izquierda");
    }
    if (orientacion == 1) {
        var primerItemActivo = document.getElementsByClassName("activo")[0];
        primerItemActivo.classList.replace("activo", "inactivo");
        listaUl.appendChild(primerItemActivo);
        var primerItemInactivo = document.getElementsByClassName("inactivo")[0];
        primerItemInactivo.classList.replace("inactivo", "activo");
        primerItemInactivo.classList.add("derecha");

    } else {
        var ultimoItemActivo = document.getElementsByClassName("activo")[0];
        ultimoItemActivo.classList.replace("activo", "inactivo");
        var ultimoItemInactivo = document.getElementsByClassName("inactivo")[lista.length - 1];
        ultimoItemInactivo.classList.replace("inactivo", "activo");
        ultimoItemInactivo.classList.add("izquierda");
        listaUl.insertBefore(ultimoItemInactivo, listaUl.firstChild);
    }
}

function timerMover() {
    clearInterval(timer);
    timer = setInterval(function () {
        moverSlide(1);
    }, 5000);
}
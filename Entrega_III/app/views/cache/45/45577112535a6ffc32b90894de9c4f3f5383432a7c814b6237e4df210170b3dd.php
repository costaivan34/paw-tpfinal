<?php

/* partials/headerNav.html */
class __TwigTemplate_15a44dbda2484460759d6411b86670188c893831d3c59fd2911ffcbeae712960 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<section class=\"headerNav\">
    <nav>
        <ul>
            <li><a href=\"/\">Home</a></li>
            <li><a href=\"/resto\">Restaurantes</a></li>
            <li><a href=\"/platos\">Platos</a></li>
            <li><a href=\"/users\">Bebidas</a></li>
            <li><a href=\"/tasks\">Cerca de mi</a></li>
    
        </ul>
    </nav>
</section>";
    }

    public function getTemplateName()
    {
        return "partials/headerNav.html";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<section class=\"headerNav\">
    <nav>
        <ul>
            <li><a href=\"/\">Home</a></li>
            <li><a href=\"/resto\">Restaurantes</a></li>
            <li><a href=\"/platos\">Platos</a></li>
            <li><a href=\"/users\">Bebidas</a></li>
            <li><a href=\"/tasks\">Cerca de mi</a></li>
    
        </ul>
    </nav>
</section>", "partials/headerNav.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\partials\\headerNav.html");
    }
}

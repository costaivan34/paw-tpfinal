<?php

/* partials/mainTopRestaurantes.html */
class __TwigTemplate_88dae50513e137a17409fa668edbf4664986a040702bee263b84b407a23a8b1b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--esto claramente sacado de la base de datos va a ser dinamico-->
<section id=\"topRestaurantes\">
    <h2>TOP 3 RESTAURANTES</h2>
    <ul>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 1</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 2</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 3</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
    </ul>
</section>";
    }

    public function getTemplateName()
    {
        return "partials/mainTopRestaurantes.html";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--esto claramente sacado de la base de datos va a ser dinamico-->
<section id=\"topRestaurantes\">
    <h2>TOP 3 RESTAURANTES</h2>
    <ul>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 1</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 2</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopRestaurant\">
            <a href=\"/\">
                <img src=\"/public/res/restaurant.jpg\" alt=\"imagen restaurant\" class=\"fotoRestaurant\">
                <section class=\"titulos\">
                    <h3>Restaurante 3</h3>
                    <h4>Ubicacion: Ubicacion X</h4>
                </section>
            </a>
        </li>
    </ul>
</section>", "partials/mainTopRestaurantes.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\partials\\mainTopRestaurantes.html");
    }
}

<?php

/* partials/mainTopPlatos.html */
class __TwigTemplate_b9e95378b813825f6272409f42733a9646c24e557754810aceec9e6431557b77 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--esto claramente sacado de la base de datos va a ser dinamico-->
<section id=\"topPlatos\">
    <h2>TOP 3 PLATOS</h2>
    <ul>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 1</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 2</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 3</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
    </ul>
</section>";
    }

    public function getTemplateName()
    {
        return "partials/mainTopPlatos.html";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--esto claramente sacado de la base de datos va a ser dinamico-->
<section id=\"topPlatos\">
    <h2>TOP 3 PLATOS</h2>
    <ul>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 1</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 2</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
        <li class=\"tarjetaTopPlato\">
            <a href=\"/\">
                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                <section class=\"titulos\">
                    <h3>Plato 3</h3>
                    <h4>Restaurant: Restaurant X </h4>
                </section>
            </a>
        </li>
    </ul>
</section>", "partials/mainTopPlatos.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\partials\\mainTopPlatos.html");
    }
}

<?php

/* buscaPlato.html */
class __TwigTemplate_d37b468a9731d7e7d2a7bfed4feadb9453a9eeda15e19397e31f3b0e2862b785 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "buscaPlato.html", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'head' => array($this, 'block_head'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Buscar Platos";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo twig_include($this->env, $context, "partials/headerIntro.html");
        echo "
";
        // line 7
        echo twig_include($this->env, $context, "partials/headerNav.html");
        echo "

";
    }

    // line 11
    public function block_head($context, array $blocks = array())
    {
        // line 12
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<meta name=\"keywords\" content=\"PAW,2018,Templates,PHP\">
";
    }

    // line 16
    public function block_main($context, array $blocks = array())
    {
        // line 17
        echo twig_include($this->env, $context, "partials/headerBuscador.html");
        echo "
<section  id=\"sectionDatos\">
        <ul id=\"busquedaResto\">
            <li class=\"tarjetabusqueda\">
                    <a href=\"/\">
                        <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                        <section class=\"titulosR\">
                            <h4>Restaurant: Restaurant X </h4>
                            <h4>Ubicacion: Lujan </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        </section>
                    </a>
                </li>
                <li class=\"tarjetabusqueda\">
                        <a href=\"/\">
                            <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                            <section class=\"titulosR\">
                                <h4>Restaurant: Restaurant X </h4>
                                <h4>Ubicacion: Lujan </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            </section>
                        </a>
                    </li>
                    <li class=\"tarjetabusqueda\">
                            <a href=\"/\">
                                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                <section class=\"titulosR\">
                                    <h4>Restaurant: Restaurant X </h4>
                                    <h4>Ubicacion: Lujan </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </section>
                            </a>
                        </li>
                        <li class=\"tarjetabusqueda\">
                                <a href=\"/\">
                                    <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                    <section class=\"titulosR\">
                                        <h4>Restaurant: Restaurant X </h4>
                                        <h4>Ubicacion: Lujan </h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    </section>
                                </a>
                            </li>
                            <li class=\"tarjetabusqueda\">
                                    <a href=\"/\">
                                        <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                        <section class=\"titulosR\">
                                            <h4>Restaurant: Restaurant X </h4>
                                            <h4>Ubicacion: Lujan </h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        </section>
                                    </a>
                                </li>
                                <li class=\"tarjetabusqueda\">
                                        <a href=\"/\">
                                            <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                            <section class=\"titulosR\">
                                                <h4>Restaurant: Restaurant X </h4>
                                                <h4>Ubicacion: Lujan </h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                            </section>
                                        </a>
                                    </li>
                        
            </ul>
    </section>
<form id=\"filtros\">

        <h2>Filtros de Busqueda</h2>
       
        <section class=\"filtro\">
                <h2>Ordenar por:
                <select name=\"cars\">
                        <option value=\"volvo\">Volvo</option>
                        <option value=\"saab\">Saab</option>
                        <option value=\"fiat\">Fiat</option>
                        <option value=\"audi\">Audi</option>
                      </select>
                </section>
            </h2>

                <section class=\"filtro\">
                <h2>Zonas</h2>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                </section>
           
         
                <section class=\"filtro\">
                        <h2>Caracteristicas</h2>
                        <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                </section>
             
          
                <section class=\"filtro\">
                <h2>Precio</h2>
                0<input type=\"range\"  id=\"a\" name=\"a\" value=\"50\">100 
                
                </section>
       
   
        <input type=\"submit\">
</form>




";
    }

    public function getTemplateName()
    {
        return "buscaPlato.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 17,  65 => 16,  58 => 12,  55 => 11,  48 => 7,  44 => 6,  41 => 5,  35 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html\" %}

{% block title %}Buscar Platos{% endblock %}

{% block header %}
{{ include('partials/headerIntro.html') }}
{{ include('partials/headerNav.html') }}

{% endblock %}

{% block head %}
{{ parent() }}
<meta name=\"keywords\" content=\"PAW,2018,Templates,PHP\">
{% endblock %}

{% block main %}
{{ include('partials/headerBuscador.html') }}
<section  id=\"sectionDatos\">
        <ul id=\"busquedaResto\">
            <li class=\"tarjetabusqueda\">
                    <a href=\"/\">
                        <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                        <section class=\"titulosR\">
                            <h4>Restaurant: Restaurant X </h4>
                            <h4>Ubicacion: Lujan </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        </section>
                    </a>
                </li>
                <li class=\"tarjetabusqueda\">
                        <a href=\"/\">
                            <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                            <section class=\"titulosR\">
                                <h4>Restaurant: Restaurant X </h4>
                                <h4>Ubicacion: Lujan </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            </section>
                        </a>
                    </li>
                    <li class=\"tarjetabusqueda\">
                            <a href=\"/\">
                                <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                <section class=\"titulosR\">
                                    <h4>Restaurant: Restaurant X </h4>
                                    <h4>Ubicacion: Lujan </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </section>
                            </a>
                        </li>
                        <li class=\"tarjetabusqueda\">
                                <a href=\"/\">
                                    <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                    <section class=\"titulosR\">
                                        <h4>Restaurant: Restaurant X </h4>
                                        <h4>Ubicacion: Lujan </h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    </section>
                                </a>
                            </li>
                            <li class=\"tarjetabusqueda\">
                                    <a href=\"/\">
                                        <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                        <section class=\"titulosR\">
                                            <h4>Restaurant: Restaurant X </h4>
                                            <h4>Ubicacion: Lujan </h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        </section>
                                    </a>
                                </li>
                                <li class=\"tarjetabusqueda\">
                                        <a href=\"/\">
                                            <img src=\"/public/res/plato.jpg\" alt=\"imagen restaurant\" class=\"fotoPlato\">
                                            <section class=\"titulosR\">
                                                <h4>Restaurant: Restaurant X </h4>
                                                <h4>Ubicacion: Lujan </h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                            </section>
                                        </a>
                                    </li>
                        
            </ul>
    </section>
<form id=\"filtros\">

        <h2>Filtros de Busqueda</h2>
       
        <section class=\"filtro\">
                <h2>Ordenar por:
                <select name=\"cars\">
                        <option value=\"volvo\">Volvo</option>
                        <option value=\"saab\">Saab</option>
                        <option value=\"fiat\">Fiat</option>
                        <option value=\"audi\">Audi</option>
                      </select>
                </section>
            </h2>

                <section class=\"filtro\">
                <h2>Zonas</h2>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                </section>
           
         
                <section class=\"filtro\">
                        <h2>Caracteristicas</h2>
                        <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                <input type=\"checkbox\" name=\"vehicle1\" value=\"Bike\"> <br>
                <input type=\"checkbox\" name=\"vehicle2\" value=\"Car\"> <br>
                </section>
             
          
                <section class=\"filtro\">
                <h2>Precio</h2>
                0<input type=\"range\"  id=\"a\" name=\"a\" value=\"50\">100 
                
                </section>
       
   
        <input type=\"submit\">
</form>




{% endblock %}", "buscaPlato.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\buscaPlato.html");
    }
}

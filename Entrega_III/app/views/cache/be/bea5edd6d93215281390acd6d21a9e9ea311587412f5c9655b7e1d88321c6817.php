<?php

/* partials/headerIntro.html */
class __TwigTemplate_8cae17256142a6f92f306e8897ecbd3f06fe578d55c00c53de6ffc2e75df0fe7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"headerIntro\">
        <ul>
   <li><a href=\"/\"><img src=\"/public/res/logo.jpg\" alt=\"logo de página\" id=\"logoPagina\"> </a>
   <!-- <h1>NOMBRE DEL SITIO</h1>-->
   <li>  <a href=\"http://www.facebook.com\"><img src=\"/public/res/logoFacebook.png\" alt=\"logo de facebook\"
            class=\"logoRedesSociales\"></a></li>
            <li> <a href=\"http://www.twitter.com\"><img src=\"/public/res/logoTwitter.png\" alt=\"logo de twitter\"
            class=\"logoRedesSociales\"></a></li>
            <li><a href=\"http://www.instagram.com\"><img src=\"/public/res/logoInstagram.png\" alt=\"logo de instagram\"
            class=\"logoRedesSociales\"></a></li>
            <li> <section class=\"headerIntroBuscador\"></section>
                <form action=\"/\">
                    <input type=\"search\" name=\"buscador\" id=\"busca\" placeholder=\"Ej.: Starbuck, Parrilla el Beto, Sushi..\"
                        required>
            
                    <input type=\"submit\" value=\"Buscar\" id=\"botonBusca\">
                </form>
            </section></li>
            <li><a href=\"/login\" id=\"login\"><span>Ingresar</span><img src=\"/public/res/logoLogin.png\" alt=\"logo de login\"
            class=\"login\"></a></li>
        </ul>
</section>";
    }

    public function getTemplateName()
    {
        return "partials/headerIntro.html";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"headerIntro\">
        <ul>
   <li><a href=\"/\"><img src=\"/public/res/logo.jpg\" alt=\"logo de página\" id=\"logoPagina\"> </a>
   <!-- <h1>NOMBRE DEL SITIO</h1>-->
   <li>  <a href=\"http://www.facebook.com\"><img src=\"/public/res/logoFacebook.png\" alt=\"logo de facebook\"
            class=\"logoRedesSociales\"></a></li>
            <li> <a href=\"http://www.twitter.com\"><img src=\"/public/res/logoTwitter.png\" alt=\"logo de twitter\"
            class=\"logoRedesSociales\"></a></li>
            <li><a href=\"http://www.instagram.com\"><img src=\"/public/res/logoInstagram.png\" alt=\"logo de instagram\"
            class=\"logoRedesSociales\"></a></li>
            <li> <section class=\"headerIntroBuscador\"></section>
                <form action=\"/\">
                    <input type=\"search\" name=\"buscador\" id=\"busca\" placeholder=\"Ej.: Starbuck, Parrilla el Beto, Sushi..\"
                        required>
            
                    <input type=\"submit\" value=\"Buscar\" id=\"botonBusca\">
                </form>
            </section></li>
            <li><a href=\"/login\" id=\"login\"><span>Ingresar</span><img src=\"/public/res/logoLogin.png\" alt=\"logo de login\"
            class=\"login\"></a></li>
        </ul>
</section>", "partials/headerIntro.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\partials\\headerIntro.html");
    }
}

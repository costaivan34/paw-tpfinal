<?php

/* index.html */
class __TwigTemplate_c46ab31beca99da74ea3202ea2c353a38e43691388ce6d6894d75c4fba6568c5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "index.html", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'head' => array($this, 'block_head'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Home";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo twig_include($this->env, $context, "partials/headerIntro.html");
        echo "
";
        // line 7
        echo twig_include($this->env, $context, "partials/headerNav.html");
        echo "

";
    }

    // line 11
    public function block_head($context, array $blocks = array())
    {
        // line 12
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<meta name=\"keywords\" content=\"PAW,2018,Templates,PHP\">
";
    }

    // line 16
    public function block_main($context, array $blocks = array())
    {
        // line 17
        echo twig_include($this->env, $context, "partials/mainCarrousel.html");
        echo "

";
        // line 19
        echo twig_include($this->env, $context, "partials/mainTopRestaurantes.html");
        echo "
";
        // line 20
        echo twig_include($this->env, $context, "partials/mainCarrousel.html");
        echo "
";
        // line 21
        echo twig_include($this->env, $context, "partials/mainTopPlatos.html");
        echo "
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 21,  77 => 20,  73 => 19,  68 => 17,  65 => 16,  58 => 12,  55 => 11,  48 => 7,  44 => 6,  41 => 5,  35 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html\" %}

{% block title %}Home{% endblock %}

{% block header %}
{{ include('partials/headerIntro.html') }}
{{ include('partials/headerNav.html') }}

{% endblock %}

{% block head %}
{{ parent() }}
<meta name=\"keywords\" content=\"PAW,2018,Templates,PHP\">
{% endblock %}

{% block main %}
{{include ('partials/mainCarrousel.html')}}

{{include ('partials/mainTopRestaurantes.html')}}
{{include ('partials/mainCarrousel.html')}}
{{include ('partials/mainTopPlatos.html')}}
{% endblock %}", "index.html", "C:\\Users\\user\\Documents\\paw-tpfinal\\Entrega_III\\app\\views\\index.html");
    }
}

<?php

namespace App\Controllers;

class PagesController
{
    /**
     * Show the home page.
     */
    
    public function home()
    {
        return view('index');
    }

    public function platos()
    {
        return view('buscaPlato');
    }

    /**
     * Show the about page.
     */
    public function about()
    {
        $company = 'Laracasts';

        return view('about', ['company' => $company]);
    }

    /**
     * Show the contact page.
     */
    public function contact()
    {
        return view('contact');
    }

    public function resto()
    {
        return view('buscaResto');
    }

}

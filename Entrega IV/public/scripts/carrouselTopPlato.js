var sectionBase = sectionBase || {},
    timer,
    lista = [];

armarCarrouselTopP = function (contenedor) {
    window.addEventListener("DOMContentLoaded", function () {
        sectionBase = document.getElementById(contenedor);
        cargarTopPlato();
        crearNavegadorTopP();
        timerMoverTopP();
    });
};

//aca vendrian los datos por parametros y se completa dinamicamente
function cargarTopPlato() {
    lista = [{
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 1",
            "restaurante": "Restaurante 1",
            "id": 0,
        },
        {
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 2",
            "restaurante": "Restaurante 2",
            "id": 1,
        },
        {
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 3",
            "restaurante": "Restaurante 3",
            "id": 2,
        },
        {
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 4",
            "restaurante": "Restaurante 4",
            "id": 3,
        },
        {
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 5",
            "restaurante": "Restaurante 5",
            "id": 4,
        },
        {
            "foto": "./public/res/plato.jpg",
            "nombre": "Plato 6",
            "restaurante": "Restaurante 6",
            "id": 5,
        }
    ];
}

function crearNavegadorTopP() {
    var h2Titulo = document.createElement("h2");
    h2Titulo.textContent = "Top Platos";
    var listaTops = document.createElement("ul");
    listaTops.id = "listaTopPlatos";
    for (let i = 0; i < lista.length; i++) {
        var itemLI = document.createElement("li");
        itemLI.classList.add("liCarrouselPlato");
        var aItem = document.createElement("a");
        aItem.setAttribute("href", "/plato?id=" + i);
        var item = document.createElement("img");
        item.classList.add("fotoTops");
        item.setAttribute("src", lista[i]["foto"]);
        var h3Descripcion = document.createElement("h3");
        h3Descripcion.textContent = lista[i]["nombre"];
        var h3Restaurante = document.createElement("h3");
        h3Restaurante.textContent = lista[i]["restaurante"];
        if (i < 3) {
            itemLI.classList.add("activoTopPlato");
        } else {
            itemLI.classList.add("inactivoTopPlato");
        }

        aItem.appendChild(item);
        aItem.appendChild(h3Descripcion);
        aItem.appendChild(h3Restaurante);
        itemLI.appendChild(aItem);
        listaTops.appendChild(itemLI);
    }

    //creo botones prev next
    var aPrev = document.createElement("a");
    aPrev.innerHTML = "&#10094;";
    aPrev.classList.add("prev");
    aPrev.addEventListener("click", function () {
        timerMoverTopP();
        moverSlideTopP(-1);
    });
    var aNext = document.createElement("a");
    aNext.innerHTML = "&#10095;";
    aNext.classList.add("next");
    aNext.addEventListener("click", function () {
        timerMoverTopP();
        moverSlideTopP(1);
    });
    sectionBase.appendChild(h2Titulo);
    sectionBase.appendChild(aPrev);
    sectionBase.appendChild(listaTops);
    sectionBase.appendChild(aNext);
}

function moverSlideTopP(orientacion) {
    var listaUl = document.getElementById("listaTopPlatos");
    var listaLi = document.getElementsByClassName("liCarrouselPlato");
    for (let i = 0; i < listaLi.length; i++) {
        listaLi[i].classList.remove("derecha");
        listaLi[i].classList.remove("izquierda");
    }
    if (orientacion == 1) {
        var primerItemActivo = document.getElementsByClassName("activoTopPlato")[0];
        primerItemActivo.classList.replace("activoTopPlato", "inactivoTopPlato");
        listaUl.appendChild(primerItemActivo);
        var primerItemInactivo = document.getElementsByClassName("inactivoTopPlato")[0];
        primerItemInactivo.classList.replace("inactivoTopPlato", "activoTopPlato");
        primerItemInactivo.classList.add("derecha");

    } else {
        var ultimoItemActivo = document.getElementsByClassName("activoTopPlato")[2];
        ultimoItemActivo.classList.replace("activoTopPlato", "inactivoTopPlato");
        var ultimoItemInactivo = document.getElementsByClassName("inactivoTopPlato")[2];
        ultimoItemInactivo.classList.replace("inactivoTopPlato", "activoTopPlato");
        ultimoItemInactivo.classList.add("izquierda");
        listaUl.insertBefore(ultimoItemInactivo, listaUl.firstChild);
    }
}

function timerMoverTopP() {
    clearInterval(timer);
    // timer = setInterval(function () {
    //     moverSlideTopP(1);
    //}, 5000);
}
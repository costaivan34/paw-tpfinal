function winClickDrop(e) {
    let dropdown = document.querySelector('.dropdown');
    let target = e.target;
    if (target.closest('.dropdown') == null) {
        dropdown.classList.remove('activo');
    }
}

function eventos() {
    let modalFiltros = document.getElementById('modalFiltros');
    modalFiltros.addEventListener('click', function (e) {
        let acordeonHeader = e.target.closest('.acordeonHeader');
        if (acordeonHeader != null) {
            acordeonHeader.classList.toggle('activo');
            acordeonHeader.nextElementSibling.classList.toggle('activo');
        }
    })

    var dropdown = document.querySelector('.dropdown');
    dropdown.addEventListener('click', function () {
        this.classList.toggle('activo');
    })
    window.removeEventListener('click', winClickDrop);
    window.addEventListener('click', winClickDrop);
}
window.addEventListener('load', function () {
    let miMainMobile = document.getElementsByTagName('main')[0].innerHTML;
    eventos();
    const mediaQuery = window.matchMedia('(min-width: 768px)');
    detectResize(mediaQuery); // Initial check
    mediaQuery.addEventListener("change", detectResize); // Register event listener
    function detectResize() {
        if (mediaQuery.matches) {
            let containerSuperior = document.querySelector('main .container');
            let divContainerCards = document.querySelector('.containerCards');
            let divContainerFilterAndElse = document.createElement('div');
            divContainerFilterAndElse.classList.add('containerFilterAndElse');
            let divContainerCardsAndHeader = document.createElement('div');
            divContainerCardsAndHeader.classList.add('containerCardsAndHeader');
            modalFiltros.classList.remove('modal');
            let closeBtn = modalFiltros.querySelector('.closeBtn');
            if (closeBtn != null) {
                closeBtn.parentNode.removeChild(closeBtn);
            }
            let allAcordeonHeader = modalFiltros.querySelectorAll('.acordeonHeader');
            for (const singleAcordeon of allAcordeonHeader) {
                singleAcordeon.click();
            }
            let dropdown = document.querySelector('.containerFiltros .dropdown');
            let containerHeader = document.createElement('div');
            containerHeader.classList.add('containerCardsHeader');
            let pMostrando = document.getElementById('mostrando');
            containerHeader.appendChild(pMostrando);
            containerHeader.appendChild(dropdown);
            let containerFiltros = document.querySelector('.containerFiltros');
            containerFiltros.parentNode.removeChild(containerFiltros);
            divContainerCardsAndHeader.appendChild(containerHeader);
            divContainerCardsAndHeader.appendChild(divContainerCards);
            divContainerFilterAndElse.appendChild(modalFiltros);
            divContainerFilterAndElse.appendChild(divContainerCardsAndHeader);
            containerSuperior.appendChild(divContainerFilterAndElse);
        } else {
            document.getElementsByTagName('main')[0].innerHTML = miMainMobile;
            document.getElementById('btnModalFiltros').addEventListener('click', eventoModal);
            eventos();
        }
    }
});
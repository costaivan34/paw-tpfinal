/*MODAL*/

function winClickModal(e) {
    if (e.target.closest('.modal') != null && e.target.closest('.container') == null) {
        e.target.closest('.modal').classList.remove('activo');
    }
}
function eventoModal() {
    let modal = document.querySelector(this.dataset.target);
    modal.classList.add('activo');
    let closeBtn = modal.querySelector('.closeBtn');
    closeBtn.addEventListener('click', function () {
        modal.classList.remove('activo');
    });
    window.removeEventListener('click', winClickModal);
    window.addEventListener('click', winClickModal);
}

window.addEventListener('load', function () {
    let sectionFormIngresar = document.getElementById('sectionFormIngresar')
    let sectionFormRegistrarse = document.getElementById('sectionFormRegistrarse');
    let btnModalIngresar = document.getElementById('btnModalIngresar');
    let btnModalRegistrarse = document.getElementById('btnModalRegistrarse');
    function clickIngresar() {
        btnModalIngresar.classList.add('activo');
        btnModalRegistrarse.classList.remove('activo');
        sectionFormIngresar.classList.replace('slideRegistrate', 'slideIngresar');
        sectionFormRegistrarse.classList.replace('slideRegistrate', 'slideIngresar');
    };

    function clickRegistrate() {
        btnModalRegistrarse.classList.add('activo');
        btnModalIngresar.classList.remove('activo');
        sectionFormIngresar.classList.replace('slideIngresar', 'slideRegistrate');
        sectionFormRegistrarse.classList.replace('slideIngresar', 'slideRegistrate');
    };

    btnModalIngresar.addEventListener('click', clickIngresar);
    btnModalRegistrarse.addEventListener('click', clickRegistrate);

    /*MODAL*/
    var todosBtnOpenModal = document.querySelectorAll("[data-toggle='modal']");
    for (const btn of todosBtnOpenModal) {
        btn.addEventListener('click', eventoModal);
    }

    /*HEADER*/

    var sanguche = document.getElementById('sanguche');
    var nav = document.getElementsByTagName('nav')[0];
    sanguche.addEventListener('click', function (e) {
        e.stopPropagation();
        nav.classList.add('activo');
        document.body.classList.add('menuOpen');
    })
    var searchIcon = document.getElementById('headerSearch');
    var headerBuscarSection = document.getElementById('headerBuscarSection');
    searchIcon.addEventListener('click', function () {
        headerBuscarSection.classList.add('activo');
    });
    var searchBack = document.getElementById('searchBack');
    searchBack.addEventListener('click', function () {
        headerBuscarSection.classList.remove('activo');
    });

    var prevScrollpos = window.pageYOffset;
    var header = document.getElementsByTagName("header")[0];
    window.addEventListener('scroll', function () {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            header.classList.remove('hide');
        } else {
            header.classList.add('hide');
        }
        prevScrollpos = currentScrollPos;
    });


    /*NAV*/

    var nav = document.getElementsByTagName('nav')[0];
    var navContainer = document.querySelector('nav .container');
    var closeBtnNav = document.getElementById('closeBtnNav');
    function cerrarNav() {
        nav.classList.remove('activo');
        document.body.classList.remove('menuOpen');
    };
    closeBtnNav.addEventListener('click', cerrarNav);

    var btnIngresar = document.getElementById('btnIngresar');
    var btnHeaderIngresar = document.getElementById('btnHeaderIngresar');
    var btnRegistrate = document.getElementById('btnRegistrate');
    var btnHeaderRegistrate = document.getElementById('btnHeaderRegistrate');

    btnIngresar.addEventListener('click', function () {
        clickIngresar();
    });
    btnHeaderIngresar.addEventListener('click', function () {
        clickIngresar();
    });
    btnRegistrate.addEventListener('click', function () {
        clickRegistrate();
    });
    btnHeaderRegistrate.addEventListener('click', function () {
        clickRegistrate();
    });
    window.addEventListener('click', function (e) {
        if (e.target != nav && e.target != navContainer) {
            cerrarNav();
        }
    })

    /*FOOTER*/

    var goToCielo = document.getElementById('goToCielo');
    goToCielo.addEventListener('click', function () {
        window.scrollTo(0, 0);
    })
});
var sectionBase = sectionBase || {},
    timer,
    lista = [];

armarCarrouselTopR = function (contenedor) {
    window.addEventListener("DOMContentLoaded", function () {
        sectionBase = document.getElementById(contenedor);
        cargarTopRestaurantes();
        crearNavegadorTopR();
        timerMoverTopR();
    });
};

//aca vendrian los datos por parametros y se completa dinamicamente
function cargarTopRestaurantes() {
    lista = [{
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 1",
            "ubicacion": "Ubicacion 1",
            "id": 0,
        },
        {
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 2",
            "ubicacion": "Ubicacion 2",
            "id": 1,
        },
        {
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 3",
            "ubicacion": "Ubicacion 3",
            "id": 2,
        },
        {
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 4",
            "ubicacion": "Ubicacion 4",
            "id": 3,
        },
        {
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 5",
            "ubicacion": "Ubicacion 5",
            "id": 4,
        },
        {
            "foto": "./public/res/restaurant.jpg",
            "nombre": "Restaurante 6",
            "ubicacion": "Ubicacion 6",
            "id": 6,
        }
    ];
}

function crearNavegadorTopR() {
    var h2Titulo = document.createElement("h2");
    h2Titulo.textContent = "Top Restaurantes";
    var listaTops = document.createElement("ul");
    listaTops.id = "listaTopRest";
    for (let i = 0; i < lista.length; i++) {
        var itemLI = document.createElement("li");
        itemLI.classList.add("liCarrouselRest");
        var aItem = document.createElement("a");
        aItem.setAttribute("href", "/restaurante?id=" + i);
        var item = document.createElement("img");
        item.classList.add("fotoTops");
        item.setAttribute("src", lista[i]["foto"]);
        var h3Descripcion = document.createElement("h3");
        h3Descripcion.textContent = lista[i]["nombre"];
        var h3Ubicacion = document.createElement("h3");
        h3Ubicacion.textContent = lista[i]["ubicacion"];
        if (i < 3) {
            itemLI.classList.add("activoTopRest");
        } else {
            itemLI.classList.add("inactivoTopRest");
        }

        aItem.appendChild(item);
        aItem.appendChild(h3Descripcion);
        aItem.appendChild(h3Ubicacion);
        itemLI.appendChild(aItem);
        listaTops.appendChild(itemLI);
    }

    //creo botones prev next
    var aPrev = document.createElement("a");
    aPrev.innerHTML = "&#10094;";
    aPrev.classList.add("prev");
    aPrev.addEventListener("click", function () {
        timerMoverTopR();
        moverSlideTopR(-1);
    });
    var aNext = document.createElement("a");
    aNext.innerHTML = "&#10095;";
    aNext.classList.add("next");
    aNext.addEventListener("click", function () {
        timerMoverTopR();
        moverSlideTopR(1);
    });
    sectionBase.appendChild(h2Titulo);
    sectionBase.appendChild(aPrev);
    sectionBase.appendChild(listaTops);
    sectionBase.appendChild(aNext);
}

function moverSlideTopR(orientacion) {
    var listaUl = document.getElementById("listaTopRest");
    var listaLi = document.getElementsByClassName("liCarrouselRest");
    for (let i = 0; i < listaLi.length; i++) {
        listaLi[i].classList.remove("derecha");
        listaLi[i].classList.remove("izquierda");
    }
    if (orientacion == 1) {
        var primerItemActivo = document.getElementsByClassName("activoTopRest")[0];
        primerItemActivo.classList.replace("activoTopRest", "inactivoTopRest");
        listaUl.appendChild(primerItemActivo);
        var primerItemInactivo = document.getElementsByClassName("inactivoTopRest")[0];
        primerItemInactivo.classList.replace("inactivoTopRest", "activoTopRest");
        primerItemInactivo.classList.add("derecha");

    } else {
        var ultimoItemActivo = document.getElementsByClassName("activoTopRest")[2];
        ultimoItemActivo.classList.replace("activoTopRest", "inactivoTopRest");
        var ultimoItemInactivo = document.getElementsByClassName("inactivoTopRest")[2];
        ultimoItemInactivo.classList.replace("inactivoTopRest", "activoTopRest");
        ultimoItemInactivo.classList.add("izquierda");
        listaUl.insertBefore(ultimoItemInactivo, listaUl.firstChild);
    }
}

function timerMoverTopR() {
    clearInterval(timer);
    // timer = setInterval(function () {
    //     moverSlideTopR(1);
    //}, 5000);
}
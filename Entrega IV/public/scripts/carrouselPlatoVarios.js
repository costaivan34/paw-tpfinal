var sectionBaseVarios = sectionBaseVarios || {},
    timer,
    listaVarios = [],
    cantidadFotosVisiblesVarios = 7;

armarCarrouselPlatoVarios = function (contenedor) {
    window.addEventListener("DOMContentLoaded", function () {
        sectionBaseVarios = document.getElementById(contenedor);
        cargarFotosPlatosVarios();
        crearNavegadorPlatoVarios();
        timerMoverPlatoVarios();
    });
};

//aca vendrian los datos por parametros y se completa dinamicamente
function cargarFotosPlatosVarios() {
    listaVarios = [{
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/plato.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/plato.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        },
        {
            "foto": "./public/res/plato.jpg",
        },
        {
            "foto": "./public/res/restaurant.jpg",
        }
    ];
}

function crearNavegadorPlatoVarios() {
    var listaVariosFotosVarios = document.createElement("ul");
    listaVariosFotosVarios.id = "listaVariosFotosVarios";
    for (let i = 0; i < listaVarios.length; i++) {
        var itemVariosLIVarios = document.createElement("li");
        itemVariosLIVarios.classList.add("liCarrouselFotosVarios");
        var itemVarios = document.createElement("img");
        itemVarios.classList.add("fotoPlatoVarios");
        itemVarios.setAttribute("src", listaVarios[i]["foto"]);
        if (i == 0) {
            itemVarios.classList.add("primerFotoVarios");
            itemVariosLIVarios.classList.add("liPrimerFotoVarios");
        }
        if (i < (cantidadFotosVisiblesVarios)) {
            itemVariosLIVarios.classList.add("activoVarios");
        } else {
            itemVariosLIVarios.classList.add("inactivoVarios");
        }

        itemVariosLIVarios.appendChild(itemVarios);
        listaVariosFotosVarios.appendChild(itemVariosLIVarios);
    }

    //creo botones prev next
    var aPrevVarios = document.createElement("a");
    aPrevVarios.innerHTML = "&#10094;";
    aPrevVarios.classList.add("prevVarios");
    aPrevVarios.addEventListener("click", function () {
        timerMoverPlatoVarios();
        moverSlidePlatoVarios(-1);
    });
    var aNextVarios = document.createElement("a");
    aNextVarios.innerHTML = "&#10095;";
    aNextVarios.classList.add("nextVarios");
    aNextVarios.addEventListener("click", function () {
        timerMoverPlatoVarios();
        moverSlidePlatoVarios(1);
    });
    sectionBaseVarios.appendChild(aPrevVarios);
    sectionBaseVarios.appendChild(aNextVarios);
    sectionBaseVarios.appendChild(listaVariosFotosVarios);
}

function moverSlidePlatoVarios(orientacion) {
    var listaVariosUlVarios = document.getElementById("listaVariosFotosVarios");
    var listaVariosLiVarios = document.getElementsByClassName("liCarrouselFotosVarios");
    var primerItemVariosActivoVarios0 = document.getElementsByClassName("activoVarios")[0];
    primerItemVariosActivoVarios0.classList.remove("liPrimerFotoVarios");
    for (let i = 0; i < listaVariosLiVarios.length; i++) {
        listaVariosLiVarios[i].classList.remove("derecha");
        listaVariosLiVarios[i].classList.remove("izquierda");
    }
    if (orientacion == 1) {
        var primerItemVariosActivoVarios = document.getElementsByClassName("activoVarios")[0];
        primerItemVariosActivoVarios.classList.replace("activoVarios", "inactivoVarios");
        primerItemVariosActivoVarios.classList.remove("liPrimerFotoVarios");
        listaVariosUlVarios.appendChild(primerItemVariosActivoVarios);
        var primerItemVariosActivoVarios2 = document.getElementsByClassName("activoVarios")[0];
        primerItemVariosActivoVarios2.classList.add("liPrimerFotoVarios");
        var primerItemVariosInactivoVarios = document.getElementsByClassName("inactivoVarios")[0];
        primerItemVariosInactivoVarios.classList.replace("inactivoVarios", "activoVarios");
        primerItemVariosInactivoVarios.classList.add("derecha");
    } else {
        var ultimoItemVariosActivoVarios = document.getElementsByClassName("activoVarios")[cantidadFotosVisiblesVarios - 1]
        ultimoItemVariosActivoVarios.classList.replace("activoVarios", "inactivoVarios");
        var ultimoItemVariosInactivoVarios = document.getElementsByClassName("inactivoVarios");
        ultimoItemVariosInactivoVarios = ultimoItemVariosInactivoVarios[ultimoItemVariosInactivoVarios.length - 1];
        ultimoItemVariosInactivoVarios.classList.replace("inactivoVarios", "activoVarios");
        ultimoItemVariosInactivoVarios.classList.add("liPrimerFotoVarios");
        ultimoItemVariosInactivoVarios.classList.add("izquierda");
        listaVariosUlVarios.insertBefore(ultimoItemVariosInactivoVarios, listaVariosUlVarios.firstChild);
    }
}

function timerMoverPlatoVarios() {
    /* clearInterval(timer);
     timer = setInterval(function () {
         moverSlidePlatoVarios(1);
          }, 5000);*/
}
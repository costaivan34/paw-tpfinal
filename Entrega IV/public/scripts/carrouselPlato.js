var sectionBase = sectionBase || {},
    timer,
    lista = [],
    cantidadFotosVisibles = 1;

armarCarrouselPlato = function (contenedor) {
    window.addEventListener("DOMContentLoaded", function () {
        sectionBase = document.getElementById(contenedor);
        cargarFotosPlato();
        crearNavegadorPlato();
        timerMoverPlato();
    });
};

//aca vendrian los datos por parametros y se completa dinamicamente
function cargarFotosPlato() {
    lista = [{
            "foto": "./public/res/plato.jpg",
        },
        {
            "foto": "./public/res/plato3.jpg",
        },
        {
            "foto": "./public/res/plato2.jpg",
        },
        {
            "foto": "./public/res/plato3.jpg",
        },
        {
            "foto": "./public/res/plato.jpg",
        },
        {
            "foto": "./public/res/plato2.jpg",
        }
    ];
}

function crearNavegadorPlato() {
    var listaFotos = document.createElement("ul");
    listaFotos.id = "listaFotos";
    for (let i = 0; i < lista.length; i++) {
        var itemLI = document.createElement("li");
        itemLI.classList.add("liCarrouselFotos");
        var item = document.createElement("img");
        item.classList.add("fotoPlato");
        item.setAttribute("src", lista[i]["foto"]);
        if (i < (cantidadFotosVisibles)) {
            itemLI.classList.add("activo");
        } else {
            itemLI.classList.add("inactivo");
        }
        itemLI.appendChild(item);
        listaFotos.appendChild(itemLI);
    }

    //creo botones prev next
    var aPrev = document.createElement("a");
    aPrev.innerHTML = "&#10094;";
    aPrev.classList.add("prev");
    aPrev.addEventListener("click", function () {
        timerMoverPlato();
        moverSlidePlato(-1);
    });
    var aNext = document.createElement("a");
    aNext.innerHTML = "&#10095;";
    aNext.classList.add("next");
    aNext.addEventListener("click", function () {
        timerMoverPlato();
        moverSlidePlato(1);
    });
    sectionBase.appendChild(aPrev);
    sectionBase.appendChild(aNext);
    sectionBase.appendChild(listaFotos);
}

function moverSlidePlato(orientacion) {
    var listaUl = document.getElementById("listaFotos");
    var listaLi = document.getElementsByClassName("liCarrouselFotos");
    for (let i = 0; i < listaLi.length; i++) {
        listaLi[i].classList.remove("derecha");
        listaLi[i].classList.remove("izquierda");
    }
    if (orientacion == 1) {
        var primerItemActivo = document.getElementsByClassName("activo")[0];
        primerItemActivo.classList.replace("activo", "inactivo");
        primerItemActivo.classList.remove("liPrimerFoto");
        listaUl.appendChild(primerItemActivo);
        var primerItemInactivo = document.getElementsByClassName("inactivo")[0];
        primerItemInactivo.classList.replace("inactivo", "activo");
        primerItemInactivo.classList.add("derecha");
    } else {
        var ultimoItemActivo = document.getElementsByClassName("activo")[cantidadFotosVisibles - 1]
        ultimoItemActivo.classList.replace("activo", "inactivo");
        var ultimoItemInactivo = document.getElementsByClassName("inactivo");
        ultimoItemInactivo = ultimoItemInactivo[ultimoItemInactivo.length - 1];
        ultimoItemInactivo.classList.replace("inactivo", "activo");
        ultimoItemInactivo.classList.add("izquierda");
        listaUl.insertBefore(ultimoItemInactivo, listaUl.firstChild);
    }
}

function timerMoverPlato() {
    /* clearInterval(timer);
     timer = setInterval(function () {
         moverSlidePlato(1);
          }, 5000);*/
}
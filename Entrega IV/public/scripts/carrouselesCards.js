// https://codepen.io/whipcat/pen/PooVGdq
window.addEventListener('load', function () {
    const sliders = document.querySelectorAll('.containerCards');
    var slider_a = document.querySelectorAll('.containerCards a');

    var isMobile = false;
    if ((navigator.userAgent || navigator.vendor || window.opera).match(/Android|iPhone/i)) {
        isMobile = true;
    }

    let isDown = false;
    let startX;
    let scrollLeft;
    let x;

    var left = 0;
    var topp = 0;

    slider_a.forEach(element => {
        element.addEventListener('mousedown', function (e) {
            left = e.pageX;
            topp = e.pageY;
        });
        element.addEventListener('click', function (e) {
            if (left != e.pageX && topp != e.pageY) {
                e.stopPropagation();
                e.preventDefault();
            }
        })
    });

    function mouseDown(e, thisSlider) {
        isDown = true;
        thisSlider.classList.add('active');
        if (isMobile) {
            startX = e.targetTouches[0].pageX - thisSlider.offsetLeft;
        } else {
            startX = e.pageX - thisSlider.offsetLeft;
        }
        scrollLeft = thisSlider.scrollLeft;
    }

    function mouseUp(thisSlider) {
        isDown = false;
        thisSlider.classList.remove('active');
    }

    function mouseMove(e, thisSlider) {
        if (!isDown) return;  // stop the fn from running
        e.preventDefault();
        e.stopPropagation();
        if (isMobile) {
            x = e.targetTouches[0].pageX - thisSlider.offsetLeft;
        }
        else {
            x = e.pageX - thisSlider.offsetLeft;
        }
        const walk = (x - startX);
        thisSlider.scrollLeft = scrollLeft - walk;
    }

    sliders.forEach(element => {
        if (!isMobile) {
            element.addEventListener('mousedown', function (e) { mouseDown(e, element) });

            element.addEventListener('mouseleave', function () { mouseUp(element) });

            element.addEventListener('mouseup', function () { mouseUp(element) });

            element.addEventListener('mousemove', function (e) { mouseMove(e, element) });
        } else {
            //   touchEvents

            element.addEventListener('touchstart', function (e) { mouseDown(e, element) });

            element.addEventListener('touchend', function () { mouseUp(element) });

            element.addEventListener('touchcancel', function () { mouseUp(element) });

            element.addEventListener('touchmove', function (e) { mouseMove(e, element) });
        }
    });




    var containersSliders = document.getElementsByClassName('containerSlider');
    for (let element of containersSliders) {
        var sliderBtnLeft = element.getElementsByClassName('sliderLeft')[0];
        var sliderBtnRight = element.getElementsByClassName('sliderRight')[0];
        sliderBtnLeft.addEventListener('click', function (e) {
            e.preventDefault();
            moverSlide(-1, element);
        });
        sliderBtnRight.addEventListener('click', function (e) {
            e.preventDefault();
            moverSlide(1, element);
        });
    };

    function moverSlide(orientacion, thisContainerSlider) {
        var listaUl = thisContainerSlider.getElementsByTagName("ul")[0];
        var firstLi = listaUl.firstElementChild;
        var lastLi = listaUl.lastElementChild;
        firstLi.classList.toggle("activo");
        if (orientacion == 1) {
            listaUl.removeChild(firstLi);
            listaUl.appendChild(firstLi);
        } else {
            listaUl.removeChild(lastLi);
            listaUl.prepend(lastLi);
        }
        listaUl.firstElementChild.classList.toggle('activo');
    }
});
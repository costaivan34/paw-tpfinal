<?php

namespace App\Controllers;

use App\Core\Controller;

use App\Models\Sitio;

class SitioController extends Controller{

    public function __construct(){
        $this->model = new Sitio();
        session_start();
    }
    
    public function getOne($idSitio){
        $todosSitios = $this->model->getOne(); 
        $datos['OneSitio'] = $todosSitios;
        return view('/restaurant/restauranteSingle',compact('datos'));
    }

    public function index(){
     $todosSitios = $this->model->getAll(); 
        $datos['todosSitios'] = $todosSitios;
    /*    $datos["userLogueado"] = $_SESSION['user'];*/
        return view('/restaurant/restauranteTodos', compact('datos'));
    }
   
}

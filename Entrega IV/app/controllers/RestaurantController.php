<?php

namespace App\Controllers;

use App\Core\Controller;

use App\Models\RestaurantModel;

class RestaurantController extends Controller
{

    public function __construct()
    {
        $this->model = new RestaurantModel();
        session_start();
    }

    private $table = 'restaurant';

    public function administracionRestaurantes()
    {
        $datos = [];
        if (isset($_GET['nombre'])) {
            $datos = $this->singleRestaurant($_GET['nombre']);
            return view('/restaurant/restauranteSingle', compact('datos'));
        } else {
            return view('/restaurant/todosRestaurant', compact('datos'));
        }
    }

    private function singleRestaurant($nombre)
    {
        //buscar restaurante con ese nombre y devolver
        $infoBasica['nombre'] = $nombre;
        $infoBasica['ubicacion'] = 'Pellegrini 222';
        $infoBasica['horario'] = 'L,M,M,J,V - 10Hs-22Hs';
        $infoBasica['telefono'] = '123456789';
        $infoBasica['descripcion'] = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente totam quam voluptatum nulla provident molestiae, adipisci sit. Lorem ipsum dolor sit amet consectetur, adipisicing elit.';
        $datos['infoBasica'] = $infoBasica;
        return $datos;
    }
}
